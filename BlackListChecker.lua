--Script by Nathan Hill

local blacklist = loadstring(game:HttpGet("https://bitbucket.org/WhatIsAMilkShake/milkshake123project/raw/main/Blacklist", true))()

local name = string.lower(game:GetService("Players").LocalPlayer.Name)

for n, v in pairs(blacklist) do
    local text = blacklist[n].Text or "On Blacklist"
    local gameid = (type(blacklist[n].Game) ~= type(0) or blacklist[n].Game == game.PlaceId)
    if name == string.lower(n) and gameid then
        game:GetService("Players").LocalPlayer:Kick(text)
    end
end